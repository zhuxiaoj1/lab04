// Danilo Zhu 1943382
package inheritance;

import java.util.*;

public class BookStore {
	public static void main(String[] args) {
		Random ra = new Random();
		Book[] bookStock = new Book[5];
		
		bookStock[0] = new Book("Surviving SQL", "Seequel Oracle");
		bookStock[1] = new ElectronicBook("Functional Programming", "Hass Kell", ra.nextInt((5000 - 10) + 1) + 10);
		bookStock[2] = new Book("Why is JS hated?", "Jay Esse");
		bookStock[3] = new ElectronicBook("C#? More like Java2", "Jack Ahss", ra.nextInt((5000 - 10) + 1) + 10);
		bookStock[4] = new ElectronicBook("Trustworthy StackOverflow", "Stu Dente", ra.nextInt((5000 - 10) + 1) + 10);
	
		for(Book i : bookStock) {
			System.out.println(i);
		}
	}
}