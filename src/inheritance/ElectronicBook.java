// Danilo Zhu 1943382
package inheritance;

public class ElectronicBook extends Book {
	private int numberBytes;

	public ElectronicBook(String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}

	public String toString() {
		String superToString = super.toString();
		return superToString + ", Number of bytes used: " + numberBytes;
	}
}