// Danilo Zhu 1943382
package geometry;

import java.util.*;

public class LotsOfShapes {
	public static void main(String[] args) {
		Random ra = new Random();

		Shape[] shapeStock = new Shape[5];
		shapeStock[0] = new Rectangle(ra.nextInt((20 - 1) + 1) + 1, ra.nextInt((20 - 1) + 1) + 1);
		shapeStock[1] = new Rectangle(ra.nextInt((20 - 1) + 1) + 1, ra.nextInt((20 - 1) + 1) + 1);
		shapeStock[2] = new Circle(ra.nextInt((20 - 1) + 1) + 1);
		shapeStock[3] = new Circle(ra.nextInt((20 - 1) + 1) + 1);
		shapeStock[4] = new Square(ra.nextInt((20 - 1) + 1) + 1);

		for (Shape i : shapeStock) {
			System.out.println("Area: " + i.getArea() + ", Perimeter: " + i.getPerimeter());
		}
	}
}