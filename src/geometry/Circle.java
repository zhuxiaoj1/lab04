// Danilo Zhu 1943382
package geometry;

import java.lang.Math;

public class Circle implements Shape {
	private double radius;

	public Circle(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public double getArea() {
		return (Math.pow(radius, 2.0) * Math.PI);
	}

	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}
}