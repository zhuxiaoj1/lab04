// Danilo Zhu 1943382
package geometry;

public interface Shape {
	double getArea();

	double getPerimeter();
}